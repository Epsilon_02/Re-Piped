require('dotenv').config();

module.exports = {
  mode: 'jit',
  darkMode: 'class',
  content: ['./index.html', './src/**/*.{vue,js,ts}'],
  daisyui: {
    themes: [
      {
        rePiped: {
          ...require('daisyui/src/colors/themes')['[data-theme=forest]'],
          "--rounded-btn": "0.5rem",
        }
      },
      ...JSON.parse(process.env.THEMES),
    ],
  },
  plugins: [require('@tailwindcss/typography'), require('daisyui')],
};
