import { NextPage } from './next-page.interface';
import { RelatedStream } from './streams.interface';

export interface ChannelDetails extends NextPage<RelatedStream & { uploaded: number }> {
  id: string;
  name: string;
  avatarUrl: string;
  bannerUrl: string;
  description: string;
  subscriberCount: number;
  verified: boolean;
}

export default interface Channel {
  name: string;
  thumbnail: string;
  url: string;
  description: string;
  subscribers: number;
  videos: number;
  verified: boolean;
}
