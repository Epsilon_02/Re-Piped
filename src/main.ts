import { createApp } from 'vue';

import { HalfCircleSpinner } from 'epic-spinners';
import VueDOMPurifyHTML from 'vue-dompurify-html';
import VueFeather from 'vue-feather';

import './index.scss';
import { fluent, router } from './plugins';
import App from './App.vue';

createApp(App)
  .use(fluent)
  .use(router)
  .use(VueDOMPurifyHTML)
  .component(VueFeather.name, VueFeather)
  .component(HalfCircleSpinner.name ?? '', HalfCircleSpinner)
  .mount('#app');
