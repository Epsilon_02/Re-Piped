import ky, { Options, SearchParamsOption } from 'ky';
import { responseToCamelCase } from '@alice-health/ky-hooks-change-case';
import { RouteParams } from 'vue-router';

import Streams, { RelatedStream } from '../interfaces/streams.interface';
import { ChannelDetails } from '../interfaces/channel.interface';
import { NextPage } from '../interfaces/next-page.interface';
import { SearchResults } from '../interfaces/search-results.interface';
import VideoComments from '../interfaces/video-comment.interface';

const BASE_URL = import.meta.env.PIPED_API;

type NextPageType = 'channel' | 'comments' | 'playlists' | 'search';

const generalizeItems = (key: string) => {
  return async (_request: Request, _options: Options, response: Response) => {
    const body = await response.json();
    return new Response(
      JSON.stringify({
        ...body,
        items: body[key],
      }),
      response
    );
  };
};

const api = ky.extend({ prefixUrl: BASE_URL });

const getStream = async (videoId: string): Promise<Streams> => {
  return api.get(`streams/${videoId}`).json();
};

const getComments = async (videoId: string): Promise<VideoComments> => {
  return api
    .get(`comments/${videoId}`, {
      hooks: {
        afterResponse: [generalizeItems('comments')],
      },
    })
    .json();
};

const getNextPage = async <T extends NextPage<unknown>>(
  id: string,
  nextPage: string,
  nextPageType: NextPageType
): Promise<T> => {
  const params: SearchParamsOption = { nextpage: nextPage };
  return api
    .get(`nextpage/${nextPageType}/${id}`, {
      searchParams: params,
      hooks: {
        afterResponse: [responseToCamelCase, generalizeItems(nextPageType)],
      },
    })
    .json();
};

const search = async (query: string): Promise<SearchResults> => {
  const params: SearchParamsOption = { q: query, filter: 'all' };
  return api.get('search', { searchParams: params, hooks: { afterResponse: [responseToCamelCase] } }).json();
};

const getSearchSuggestions = async (query: string): Promise<string[]> => {
  const params: SearchParamsOption = { query };
  return api.get('suggestions', { searchParams: params }).json();
};

const getChannelDetails = async (params: RouteParams): Promise<ChannelDetails> => {
  return api
    .get(`${params.path}/${params.channelId}`, {
      hooks: {
        afterResponse: [generalizeItems('relatedStreams'), responseToCamelCase],
      },
    })
    .json();
};

const getTrending = async (region: string): Promise<RelatedStream[]> => {
  const params: SearchParamsOption = { region };
  return api.get(`trending`, { searchParams: params }).json();
};

export { getChannelDetails, getComments, getNextPage, getSearchSuggestions, getStream, getTrending, search };
