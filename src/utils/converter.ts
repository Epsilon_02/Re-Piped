// eslint-disable-next-line import/prefer-default-export
export const convert = (text: string): string => {
  return text
    .replaceAll(/(?:http(?:s)?:\/\/)?(?:www\.)?youtube\.com(\/[/a-zA-Z0-9_?=&-]*)/gm, '$1')
    .replaceAll(/(?:http(?:s)?:\/\/)?(?:www\.)?youtu\.be\/(?:watch\?v=)?([/a-zA-Z0-9_?=&-]*)/gm, '/watch?v=$1')
    .replaceAll('\n', '<br>');
};

export const capitalize = (value: string): string => {
  return value.trim().replace(/^\w/, (c) => c.toUpperCase());
};

const leadingZeroFormatter = new Intl.NumberFormat(undefined, { minimumIntegerDigits: 2 });

export const convertSecondsToDuration = (timeInSeconds: number | undefined): string => {
  if (timeInSeconds === undefined) {
    return '';
  }

  const seconds = Math.floor(timeInSeconds % 60);
  const minutes = Math.floor(timeInSeconds / 60) % 60;
  const hours = Math.floor(timeInSeconds / 3600);

  if (hours === 0) {
    return `${minutes}:${leadingZeroFormatter.format(seconds)}`;
  }
  return `${hours}:${leadingZeroFormatter.format(minutes)}:${leadingZeroFormatter.format(seconds)}`;
};
