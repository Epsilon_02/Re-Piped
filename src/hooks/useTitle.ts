export default function useTitle(title: string, suffix = true): void {
  document.title = suffix ? `${title} | Re:Piped` : title;
}
