import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/trending',
  },
  {
    path: '/trending',
    name: 'Trending',
    component: () => import('../../views/TheTrending.vue'),
  },
  {
    path: '/:path(v|w|embed|shorts|watch)/:v?',
    name: 'WatchPlayer',
    component: () => import('../../views/WatchPlayer.vue'),
  },
  {
    path: '/:path(channel|user|c)/:channelId/:videos?',
    name: 'ChannelDetails',
    component: () => import('../../views/ChannelDetails.vue'),
  },
  {
    path: '/results',
    name: 'SearchResults',
    component: () => import('../../views/SearchResults.vue'),
  },
  {
    path: '/:catchAll(.*)',
    component: () => import('../../views/ErrorView.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to) => {
  if (to.name === 'Trending' && !('region' in to.query)) {
    // TODO find a way to add the query also to the address bar
    // eslint-disable-next-line no-param-reassign
    to.query.region = 'US';
  }
});

export default router;
