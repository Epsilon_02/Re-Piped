#!/bin/sh
set -e

cd /app
./import-meta-env --example .env.example.public --output /usr/share/nginx/html/**/*

/docker-entrypoint.sh && nginx -g "daemon off;"
