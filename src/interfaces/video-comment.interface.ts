import { NextPage } from './next-page.interface';

export default interface VideoComments extends NextPage<VideoComment> {
  disabled: boolean;
}

export interface VideoComment {
  author: string;
  thumbnail: string;
  commentId: string;
  commentText: string;
  commentedTime: string;
  commentorUrl: string;
  repliesPage: string;
  likeCount: number;
  hearted: boolean;
  pinned: boolean;
  verified: boolean;
}
