export interface NextPage<T> {
  nextPage: string;
  items: T[];
}
