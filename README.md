[![status-badge](https://ci.codeberg.org/api/badges/Epsilon_02/Re-Piped/status.svg)](https://ci.codeberg.org/Epsilon_02/Re-Piped)

# Re:Piped

Another frontend for [Piped](https://github.com/TeamPiped/Piped) written from Scratch using Typescript, Vues Composition API and the vanilla HTML5 Videoplayer instead of ShakaPlayer.

## Why a new frontend?

I want to learn Vue with the composition API and love Typescript. Both things the official frontend wont have and which should be hard to migrate/integrate and maybe not everyone would be happy about it (I don't know, I didn't ask, is just assuming).
And also just out of curiosity I want to see how far I can go.
Also I want to experience with the plain HTML5 Player which have a quite nice API if you get used to it.

## Integrate in current Piped infrastructure

**Notice: it is only tested with the nginx configuration**\
Expand your `docker-compose.yml` with the following lines:

```yml
repipedfrontend:
        image: epsilon02/re-piped:latest
        restart: unless-stopped
        depends_on:
          - piped
        container_name: re-piped-frontend
        environment:
          - PIPED_API=${PIPED_API}
          - THEMES=${THEMES}
...
nginx:
        ...
        volumes:
            ...
            - ./config/pipedfrontend.conf:/etc/nginx/conf.d/pipedfrontend.conf:ro
            - ./config/repipedfrontend.conf:/etc/nginx/conf.d/repipedfrontend.conf:ro
            - ./config/ytproxy.conf:/etc/nginx/snippets/ytproxy.conf:ro
            ...
...
watchtower:
        ...
        command: re-piped-frontend piped-frontend piped-backend ytproxy varnish nginx postgres watchtower
```

Create a .env file with the following content:

```env
PIPED_API=https://pipedapi.some.domain  # for example https://pipedapi.kavin.rocks ("https://" is required)
THEMES=[]  # for example: ["light", "dark", "cyberpunk"]
```

All available themes are listed in the [`.env`](.env) file in this project.
You can also use the content of this for your own `.env` file.

Create inside the `config` folder of your piped infrastructure the file `repipedfrontend.conf` with the following content and replace `some.domain` with your own domain:

```conf
server {
    listen 80;
    server_name re-piped.some.domain;

    location / {
        proxy_pass http://repipedfrontend:80;
        proxy_http_version 1.1;
        proxy_set_header Connection "keep-alive";
    }
}
```
