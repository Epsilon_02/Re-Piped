// got this from https://stackoverflow.com/a/66605669

// type Only<T, U> = {
//   [P in keyof T]: T[P];
// } & {
//   [P in keyof U]?: never;
// };

type Only<T, U> = {
  [P in keyof T]: T[P];
} & Omit<
  {
    [P in keyof U]?: never;
  },
  keyof T
>;

type Either<T, U> = Only<T, U> | Only<U, T>;

export { Only, Either };
