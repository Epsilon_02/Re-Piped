# syntax=docker/dockerfile:1.3
FROM node:lts-alpine AS build

WORKDIR /app/

COPY . .

RUN --mount=type=cache,target=/root/.cache/yarn \
    --mount=type=cache,target=/app/node_modules \
    yarn install --prefer-offline && \
    yarn build
RUN npx pkg ./node_modules/@import-meta-env/cli/bin/import-meta-env.js -t node16-alpine -o import-meta-env

#############################################################################

FROM nginx:alpine

COPY --from=build /app/dist/ /usr/share/nginx/html/
COPY --from=build /app/import-meta-env /app/import-meta-env

COPY .env.example.public /app/.env.example.public
COPY docker/start.sh /app/start.sh
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT ["ash","/app/start.sh"]
