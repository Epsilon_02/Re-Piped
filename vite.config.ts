/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

import importMetaEnv from '@import-meta-env/unplugin';
import fluentPlugin from 'rollup-plugin-fluent-vue'
import { ViteWebfontDownload } from 'vite-plugin-webfont-dl';
import VueTypeImports from 'vite-plugin-vue-type-imports';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [
    vue(),
    VueTypeImports(),
    fluentPlugin(),
    ViteWebfontDownload(['http://fonts.cdnfonts.com/css/signika']),
    importMetaEnv.vite({ example: '.env.example.public', env: '.env' })
  ],
})
);
