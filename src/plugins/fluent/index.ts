import { FluentBundle, FluentResource } from '@fluent/bundle';
import { createFluentVue } from 'fluent-vue';

import deMessages from '../../assets/fluent/de.ftl?raw';
import enMessages from '../../assets/fluent/en.ftl?raw';

const enBundle = new FluentBundle('en');
const deBundle = new FluentBundle('de');

enBundle.addResource(new FluentResource(enMessages));
deBundle.addResource(new FluentResource(deMessages));

const fluent = createFluentVue({
  // default language
  bundles: [enBundle],
});

export { enBundle, deBundle, fluent };

export default fluent;
