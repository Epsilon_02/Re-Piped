import Channel from './channel.interface';
import { NextPage } from './next-page.interface';
import { RelatedStream } from './streams.interface';

export interface SearchResults extends NextPage<RelatedStream | Channel> {
  suggestions: unknown;
  corrected: boolean;
}
