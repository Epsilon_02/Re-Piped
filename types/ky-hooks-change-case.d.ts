declare module '@alice-health/ky-hooks-change-case' {
  import { AfterResponseHook, BeforeRequestHook } from 'ky';

  const requestToSnakeCase: BeforeRequestHook;
  const requestToCamelCase: BeforeRequestHook;
  const requestToKebabCase: BeforeRequestHook;

  const responseToSnakeCase: AfterResponseHook;
  const responseToCamelCase: AfterResponseHook;
  const responseToKebabCase: AfterResponseHook;
}
